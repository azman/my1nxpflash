/*----------------------------------------------------------------------------*/
#include "my1nxp.h"
#include "my1comlib.h"
#include "my1cons.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <unistd.h> /* for usleep */
/*----------------------------------------------------------------------------*/
#define PROGNAME "my1nxpflash"
/*----------------------------------------------------------------------------*/
#define NXP_FIND_WAIT 10000
/*----------------------------------------------------------------------------*/
#define COMMAND_NONE 0
#define COMMAND_SCAN 1
#define COMMAND_DEVICE 2
#define COMMAND_BCHECK 3
#define COMMAND_ERASE 4
#define COMMAND_WRITE 5
#define COMMAND_BWRITE 6
#define COMMAND_VERIFY 7
/*----------------------------------------------------------------------------*/
#define ERROR_GENERAL -1
#define ERROR_USER_ABORT -2
#define ERROR_PARAM_PORT -3
#define ERROR_PORT_INIT -4
#define ERROR_PORT_OPEN -5
#define ERROR_PARAM_FILE -6
#define ERROR_MULTI_CMD -7
#define ERROR_NO_COMMAND -8
#define ERROR_NO_HEXFILE -9
#define ERROR_NO_DEVICE -10
/*----------------------------------------------------------------------------*/
#define FILENAME_LEN 80
/*----------------------------------------------------------------------------*/
void about(void)
{
	printf("Use:\n  %s [options] [command]\n",PROGNAME);
	printf("Options are:\n");
	printf("  --help      : show this message. overrides other options.\n");
	printf("  --port []   : specify port number between 1-%d.\n",MAX_COM_PORT);
	printf("  --file []   : specify programming file.\n");
	printf("  --tty []    : specify name for device (useful on Linux)\n");
	printf("  --device [] : specify NXP device (not used for now).\n");
	printf("  --no-device : just as a general terminal.\n");
	printf("  --force     : force erase even when device is blank.\n");
	printf("Commands are:\n");
	printf("  scan   : Scan for available serial port.\n");
	printf("  info   : Display device information.\n");
	printf("  bcheck : Blank check device.\n");
	printf("  erase  : Erase device.\n");
	printf("  write  : Write HEX file to device.\n");
	printf("  bwrite : Like 'write' but without checking results).\n");
	printf("  verify : Verify HEX file on device.\n");
	printf("\n");
}
/*----------------------------------------------------------------------------*/
void print_portscan(ASerialPort_t* aPort)
{
	int test, cCount = 0;
	printf("--------------------\n");
	printf("COM Port Scan Result\n");
	printf("--------------------\n");
	for(test=1;test<=MAX_COM_PORT;test++)
	{
		if(check_serial(aPort,test))
		{
			printf("%s%d: ",aPort->mPortName,COM_PORT(test));
			cCount++;
			printf("Ready.\n");
		}
	}
	printf("\nDetected Port(s): %d\n\n",cCount);
}
/*----------------------------------------------------------------------------*/
int find_device(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test;
	char nxp_sync_char = aDevice ? aDevice->sync_char : NXP_SYNC_CHAR;
	aDevice->found = 0;
	/** try this once! */
	put_byte_serial(aPort,nxp_sync_char);
	wait_outgoing(aPort);
	usleep(NXP_FIND_WAIT);
	if(check_incoming(aPort))
	{
		byte_t test = get_byte_serial(aPort);
		if(test==nxp_sync_char)
		{
			aDevice->found = 1;
			return NXP_CHECK_FOUND;
		}
	}
	printf("\nReset the NXP device... ");
	test = nxp_check_isp(aPort,aDevice);
	if(test==NXP_CHECK_FOUND)
	{
		aDevice->found = 1;
		printf("NXP device found!\n\n");
	}
	else if(test==NXP_CHECK_RESET)
	{
		aDevice->found = 0;
		printf("User Reset Request!\n\n");
	}
	else /** if(test==NXP_CHECK_ABORT) */
	{
		printf("User Abort Request!\n\n");
	}
	return test;
}
/*----------------------------------------------------------------------------*/
void show_device(ADeviceNXP_t* aDevice)
{
	int boot_test;
	printf("-------------------\n");
	printf("Device Information:\n");
	printf("-------------------\n");
	printf("MFC ID: %02X\n",aDevice->mfc_id);
	printf("DEV ID: %02X\n",aDevice->dev_id);
	printf("Boot Version: %02X\n",aDevice->boot_version);
	printf("Security Bit: %02X\n",aDevice->sec_bit);
	printf("Comment:\n");
	if(aDevice->mfc_id==NXP_ID_MFG)
		printf(" [OK] Manufactured by NXP\n");
	else
		printf(" [K0] Unknown manufacurer?\n");
	switch(aDevice->dev_id)
	{
		case NXP_ID_DEV_P89V51RD2:
			printf(" [OK] Device is P89V51RD2\n");
			boot_test = NXP_BOOT_VERS_RD2;
			break;
		case NXP_ID_DEV_P89V51RB2:
			printf(" [OK] Device is P89V51RB2\n");
			boot_test = NXP_BOOT_VERS_RB2;
			break;
	}
	if(aDevice->boot_version>=boot_test)
		printf(" [OK] Bootloader version okay\n");
	else
		printf(" [KO] Bootloader version older than expected!\n");
	printf("------------------\n");
	printf("|Information Ends|\n");
	printf("------------------\n");
}
/*----------------------------------------------------------------------------*/
int bcheck_device(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test;
	printf("Device blank check... ");
	test = nxp_blank_check(aPort,aDevice);
	if(test)
	{
		printf("Cannot check device blank status!\n");
		return ERROR_GENERAL;
	}
	if(!aDevice->do_acknowledge)
	{
		byte_t temp;
		int size = strlen(aDevice->hexstr);
		while(check_incoming(aPort))
		{
			temp = get_byte_serial(aPort);
			if (size==COUNT_HEXSTR_BUFF-1)
			{
				aDevice->hexstr[size] = 0x0;
				continue;
			}
			aDevice->hexstr[size++] = temp;
		}
		aDevice->hexstr[size] = 0x0;
		printf(" [OK] Device NOT blank ('%s'){%d}!\n",aDevice->hexstr,size);
		return ERROR_GENERAL;
	}
	printf(" [OK] Device is blank!\n");
	return test;
}
/*----------------------------------------------------------------------------*/
int erase_device(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test = 0;
	/** erase if not blank! **/
	if(bcheck_device(aPort,aDevice)||aDevice->do_override)
	{
		printf("Erasing device (block0)... ");
		test = nxp_erase_block0(aPort,aDevice);
		if (test)
		{
			printf("Error sending erase commane!\n");
			return ERROR_GENERAL;
		}
		if(aDevice->do_acknowledge)
			printf(" [OK] Device erased!\n");
		else
		{
			printf(" [OK] Device NOT erased? ('%s')\n",aDevice->hexstr);
			test++;
		}
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int info_device(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test = nxp_device_info(aPort,aDevice);
	if(test)
		printf("[ERROR] Cannot get device info? (%d)\n",test);
	else
		show_device(aDevice);
	return test;
}
/*----------------------------------------------------------------------------*/
int write_device(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test = 0;
	if(!aDevice->hexfile_name)
	{
		printf("No HEX file selected!\n");
		return ERROR_NO_HEXFILE;
	}
	/* try to open file - check for existence @ open permission etc. */
	FILE *pfile = fopen(aDevice->hexfile_name,"r");
	if (!pfile)
	{
		printf("HEX file not found!\n");
		return ERROR_NO_HEXFILE;
	}
	fclose(pfile);
	if(erase_device(aPort, aDevice))
	{
		printf("Cannot erase device?!\n");
		return ERROR_GENERAL;
	}
	if (aDevice->do_blind_write) printf("[BLIND] ");
	printf("Writing HEX file '%s'... ",aDevice->hexfile_name);
	fflush(stdout);
	if (aDevice->do_blind_write)
	{
		if(!nxp_send_blind(aPort, aDevice))
			printf("Done!\n");
	}
	else
	{
		if(!nxp_send_file(aPort, aDevice))
			printf("Done!\n");
	}
	return test;
}
/*----------------------------------------------------------------------------*/
int verify_device(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test = 0;
	if(!aDevice->hexfile_name)
	{
		printf("No HEX file selected!\n");
		return ERROR_NO_HEXFILE;
	}
	/* try to open file - check for existence @ open permission etc. */
	FILE *pfile = fopen(aDevice->hexfile_name,"r");
	if (!pfile)
	{
		printf("HEX file not found!\n");
		return ERROR_NO_HEXFILE;
	}
	fclose(pfile);
	printf("Verifying HEX file '%s'... ",aDevice->hexfile_name);
	fflush(stdout);
	if(!nxp_verify_file(aPort, aDevice))
		printf("OK!\n");
	return test;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	ASerialPort_t cPort;
	ADeviceNXP_t cDevice;
	char filename[FILENAME_LEN];
	int loop, test, port=1, go_find=1, override=0;
	int do_receive = 0x1, do_command = COMMAND_NONE;
	char *pfile = 0x0, *pdev = 0x0, *ptty = 0x0;
	my1key_t key;

	/* print tool info */
	printf("\n%s - NXP Flash Tool (version %s)\n",PROGNAME,PROGVERS);
	printf("  => by azman@my1matrix.net\n\n");

	if(argc>1)
	{
		for(loop=1;loop<argc;loop++)
		{
			if(!strcmp(argv[loop],"--help")||!strcmp(argv[loop],"-h"))
			{
				about();
				return 0;
			}
			else if(!strcmp(argv[loop],"--port"))
			{
				if(get_param_int(argc,argv,&loop,&test)<0)
				{
					printf("Cannot get port number!\n");
					return ERROR_PARAM_PORT;
				}
				else if(test<1||test>MAX_COM_PORT)
				{
					printf("Invalid port number! (%d)\n", test);
					return ERROR_PARAM_PORT;
				}
				port = test;
			}
			else if(!strcmp(argv[loop],"--tty"))
			{
				if(!(ptty=get_param_str(argc,argv,&loop)))
				{
					printf("Error getting tty name!\n");
					continue;
				}
			}
			else if(!strcmp(argv[loop],"--file"))
			{
				if(!(pfile=get_param_str(argc,argv,&loop)))
				{
					printf("Error getting filename!\n");
					return ERROR_PARAM_FILE;
				}
			}
			else if(!strcmp(argv[loop],"--device"))
			{
				if(!(pdev=get_param_str(argc,argv,&loop)))
				{
					printf("Error getting device name!\n");
					continue;
				}
			}
			else if(!strcmp(argv[loop],"--no-device"))
			{
				go_find = 0;
			}
			else if(!strcmp(argv[loop],"--force"))
			{
				override = 1;
			}
			else if(!strcmp(argv[loop],"scan"))
			{
				if(do_command!=COMMAND_NONE)
				{
					printf("Multiple commands '%s'!(%d)\n",
						argv[loop],do_command);
					return ERROR_MULTI_CMD;
				}
				do_command = COMMAND_SCAN;
			}
			else if(!strcmp(argv[loop],"info"))
			{
				if(do_command!=COMMAND_NONE)
				{
					printf("Multiple commands '%s'!(%d)\n",
						argv[loop],do_command);
					return ERROR_MULTI_CMD;
				}
				do_command = COMMAND_DEVICE;
			}
			else if(!strcmp(argv[loop],"bcheck"))
			{
				if(do_command!=COMMAND_NONE)
				{
					printf("Multiple commands '%s'!(%d)\n",
						argv[loop],do_command);
					return ERROR_MULTI_CMD;
				}
				do_command = COMMAND_BCHECK;
			}
			else if(!strcmp(argv[loop],"erase"))
			{
				if(do_command!=COMMAND_NONE)
				{
					printf("Multiple commands '%s'!(%d)\n",
						argv[loop],do_command);
					return ERROR_MULTI_CMD;
				}
				do_command = COMMAND_ERASE;
			}
			else if(!strcmp(argv[loop],"write"))
			{
				if(do_command!=COMMAND_NONE)
				{
					printf("Multiple commands '%s'!(%d)\n",
						argv[loop],do_command);
					return ERROR_MULTI_CMD;
				}
				do_command = COMMAND_WRITE;
			}
			else if(!strcmp(argv[loop],"bwrite"))
			{
				if(do_command!=COMMAND_NONE)
				{
					printf("Multiple commands '%s'!(%d)\n",
						argv[loop],do_command);
					return ERROR_MULTI_CMD;
				}
				do_command = COMMAND_BWRITE;
			}
			else if(!strcmp(argv[loop],"verify"))
			{
				if(do_command!=COMMAND_NONE)
				{
					printf("Multiple commands '%s'!(%d)\n",
						argv[loop],do_command);
					return ERROR_MULTI_CMD;
				}
				do_command = COMMAND_VERIFY;
			}
			else
			{
				printf("Unknown param '%s'!\n",argv[loop]);
			}
		}
	}

	/** init device structure? */
	cDevice.found = 0;
	cDevice.do_tell_error = 1;
	cDevice.do_override = override;
	cDevice.do_verify = 0;
	cDevice.do_blind_write = 0;
	cDevice.hexfile_name = 0x0;
	cDevice.sync_char = NXP_V51_SYNC_CHAR;
	cDevice.flash_size = NXP_FLASHSIZE_P89V51RB2;

	/** initialize port */
	initialize_serial(&cPort);
#ifndef DO_MINGW
	sprintf(cPort.mPortName,"/dev/ttyUSB"); /* default on linux? */
#endif
	/** check user requested name change */
	if(ptty) sprintf(cPort.mPortName,ptty);

	/** check if user requested a port scan */
	if(do_command==COMMAND_SCAN)
	{
		print_portscan(&cPort);
		return 0;
	}

	/** try to prepare port with requested terminal */
	if(!port) port = find_serial(&cPort,0x0);
	if(!set_serial(&cPort,port))
	{
		about();
		print_portscan(&cPort);
		printf("Cannot prepare port '%s%d'!\n\n",cPort.mPortName,
			COM_PORT(port));
		return ERROR_PORT_INIT;
	}

	/** try to open port */
	if(!open_serial(&cPort))
	{
		printf("\nCannot open port '%s%d'!\n\n",cPort.mPortName,
			COM_PORT(cPort.mPortIndex));
		return ERROR_PORT_OPEN;
	}

	/** find an nxp device! */
	if(go_find&&find_device(&cPort,&cDevice)==NXP_CHECK_ABORT)
		return ERROR_USER_ABORT;

	/** check filename! */
	if(pfile) cDevice.hexfile_name = pfile;

	/** try to execute given command */
	if(do_command!=COMMAND_NONE)
	{
		if(!cDevice.found)
			return ERROR_NO_DEVICE;
		switch(do_command)
		{
			case COMMAND_DEVICE:
				info_device(&cPort,&cDevice);
				break;
			case COMMAND_BCHECK:
				bcheck_device(&cPort,&cDevice);
				break;
			case COMMAND_ERASE:
				erase_device(&cPort,&cDevice);
				break;
			case COMMAND_WRITE:
				write_device(&cPort,&cDevice);
				break;
			case COMMAND_BWRITE:
				cDevice.do_blind_write = 1;
				write_device(&cPort,&cDevice);
				cDevice.do_blind_write = 0;
				break;
			case COMMAND_VERIFY:
				verify_device(&cPort,&cDevice);
				break;
		}
		printf("\n");
		return 0; /** exit gracefully! */
	}

	printf("\nStarting Interactive Mode...\n");

	/** clear input buffer */
	purge_serial(&cPort);
	/** start main loop */
	while(1)
	{
		key = get_keyhit();
		if(key!=KEY_NONE)
		{
			if(key==KEY_ESCAPE)
			{
				printf("\nUser Exit Request! Program '%s' Ends.\n\n",PROGNAME);
				break;
			}
			else if((key&~0xFF)==0x00) /** print non-control keys */
			{
				put_byte_serial(&cPort,key);
			}
			else if(key==KEY_F1) /** show available commands */
			{
				printf("\n------------\n");
				printf("Command Keys\n");
				printf("------------\n");
				printf("<F1>  - Show this help\n");
				printf("<F2>  - Show Device Info\n");
				printf("<F3>  - Write Device (HEX)\n");
				printf("<F4>  - Verify Device (HEX)\n");
				printf("<F5>  - Black Check\n");
				printf("<F6>  - Erase Device\n");
				printf("<F7>  - Check HEX file\n");
				printf("<F8>  - Flush Port & Toggle Receive Flag\n");
				printf("<F9>  - Find Device\n");
				printf("<F10> - Reset Device (Run User Code)\n");
				printf("\n");
			}
			else if(key==KEY_F2) /** show device info */
			{
				if(find_device(&cPort,&cDevice)==NXP_CHECK_FOUND)
					info_device(&cPort,&cDevice);
			}
			else if(key==KEY_F3) /** write hexfile */
			{
				if(find_device(&cPort,&cDevice)==NXP_CHECK_FOUND)
					write_device(&cPort,&cDevice);
			}
			else if(key==KEY_F4) /** verify hexfile */
			{
				if(find_device(&cPort,&cDevice)==NXP_CHECK_FOUND)
					verify_device(&cPort,&cDevice);
			}
			else if(key==KEY_F5) /** blank check! */
			{
				if(find_device(&cPort,&cDevice)==NXP_CHECK_FOUND)
					bcheck_device(&cPort,&cDevice);
			}
			else if(key==KEY_F6) /** erase device! */
			{
				if(find_device(&cPort,&cDevice)==NXP_CHECK_FOUND)
					erase_device(&cPort,&cDevice);
			}
			else if(key==KEY_F7) /** name a HEX file */
			{
				printf("\nCurrent HEX file: '%s'\n",cDevice.hexfile_name);
				printf("Assign new HEX file name: ");
				if(!fgets(filename,FILENAME_LEN,stdin)||filename[0]=='\n')
				{
					printf("\nNo valid input!\n\n");
					continue;
				}
				cDevice.hexfile_name = filename;
				sscanf(filename,"%s",cDevice.hexfile_name);
				printf("Current HEX file: '%s'\n",cDevice.hexfile_name);
			}
			else if(key==KEY_F8) /** clear port buffer */
			{
				printf("\nFlushing input buffer!");
				purge_serial(&cPort);
				printf("\nPurging output buffer!");
				flush_serial(&cPort);
				do_receive ^= 0x01;
				if(do_receive) printf("\nTurning ON receiver!\n");
				else printf("\nTurning OFF receiver!\n");
			}
			else if(key==KEY_F9) /** find device */
			{
				if(find_device(&cPort,&cDevice)==NXP_CHECK_ABORT)
					return ERROR_USER_ABORT;
			}
			else if(key==KEY_F10)
			{
				if(find_device(&cPort,&cDevice)==NXP_CHECK_FOUND)
					nxp_reset(&cPort,&cDevice);
			}
			else
			{
				/** (key>=0x41&&key<=0x5A) / ** alpha upper */
				/** (key>=0x61&&key<=0x7A) / ** alpha lower */
				/** (key>=0x30&&key<=0x39) / ** numeric */
				/** check printable characters? */
				if(key>=0x20&&key<=0x7F)
				{
					put_byte_serial(&cPort,key); /** send */
				}
				else if(key==KEY_ENTER)
				{
					put_byte_serial(&cPort,0x0d); /** cr */
					put_byte_serial(&cPort,0x0a); /** lf */
				}
			}
		}
		if(do_receive)
		{
			/** check serial port for incoming data */
			if(check_incoming(&cPort))
			{
				byte_t cTemp = get_byte_serial(&cPort);
				putchar(cTemp);
			}
		}
	} /** end of main loop */

	close_serial(&cPort);

	return 0;
}
/*----------------------------------------------------------------------------*/
