/*----------------------------------------------------------------------------*/
#ifndef __MY1NXPH__
#define __MY1NXPH__
/*----------------------------------------------------------------------------*/
#include "my1comlib.h"
/*----------------------------------------------------------------------------*/
#define MAX_HEX_DATA_BYTE 32
#define COUNT_HEXSTR_BYTE (1+2+1+MAX_HEX_DATA_BYTE+1)
#define COUNT_HEXSTR_CHAR (1+COUNT_HEXSTR_BYTE*2+1)
#define COUNT_HEXSTR_BUFF (COUNT_HEXSTR_CHAR+2)
/*----------------------------------------------------------------------------*/
#define NXP_CHECK_EQ_INDEX 4
#define NXP_CHECK_BUFF_EXT 5
/*----------------------------------------------------------------------------*/
#define HEX_ERROR_GENERAL -1
#define HEX_ERROR_FILE -2
#define HEX_ERROR_LENGTH -3
#define HEX_ERROR_NOCOLON -4
#define HEX_ERROR_NOTDATA -5
#define HEX_ERROR_CHECKSUM -6
#define HEX_ERROR_OVERFLOW -7
#define HEX_ERROR_ACK -8
#define HEX_ERROR_INVALID -9
/*----------------------------------------------------------------------------*/
#define NXP_CHECK_START -2
#define NXP_CHECK_WAITX -1
#define NXP_CHECK_FOUND 0
#define NXP_CHECK_ABORT 1
#define NXP_CHECK_RESET 2
/*----------------------------------------------------------------------------*/
#define NXP_ERROR_GENERAL -1
#define NXP_ERROR_COMMAND -2
#define NXP_ERROR_MFCID -3
/*----------------------------------------------------------------------------*/
#define NXP_V51_SYNC_CHAR 'U'
#define NXP_SYNC_CHAR 'U'
#define NXP_ID_MFG 0xBF
#define NXP_ID_DEV_P89V51RD2 0x91
#define NXP_ID_DEV_P89V51RB2 0x9F
#define NXP_BOOT_VERS_RB2 3
#define NXP_BOOT_VERS_RD2 7
#define NXP_FLASHSIZE_P89V51RD2 0x10000
#define NXP_FLASHSIZE_P89V51RB2 0x4000
/*----------------------------------------------------------------------------*/
#define NXP_ISP_USER_CODE 0x00
#define NXP_ISP_END_OF_FILE 0x01
#define NXP_ISP_SET_SOFTICE 0x02
#define NXP_ISP_WRITE 0x03
#define NXP_ISP_CHECK 0x04
#define NXP_ISP_READ 0x05
#define NXP_ISP_LOAD_BAUDRATE 0x06
#define NXP_ISP_RESET_SERIAL 0x07
#define NXP_ISP_VERIFY_SERIAL 0x08
#define NXP_ISP_WRITE_SERIAL 0x09
#define NXP_ISP_DISPLAY_SERIAL 0x0A
#define NXP_ISP_RESET 0x0B
/*----------------------------------------------------------------------------*/
#define READ_DATA_COUNT 2
#define READ_MFG_ID 0
#define READ_DEV_ID 1
#define READ_BOOT_VER 2
#define READ_SEC_BIT 3
/*----------------------------------------------------------------------------*/
#define CHECK_DATA_COUNT 5
#define CHECK_BLANK 0
#define CHECK_DATA 1
/*----------------------------------------------------------------------------*/
#define WRITE_DATA_COUNT 3
#define WRITE_ERASE_BLOCK0 0
#define WRITE_ERASE_SECTOR 1
#define WRITE_PROGRAM_SEC 2
#define WRITE_PROGRAM_DCLK 3
/*----------------------------------------------------------------------------*/
typedef struct _linehex
{
	unsigned int count, type;
	unsigned int address;
	unsigned int lastaddr;
	unsigned int data[MAX_HEX_DATA_BYTE];
	char *hexstr;
}
ALineHEX_t;
/*----------------------------------------------------------------------------*/
typedef struct _devicenxp
{
	unsigned char sync_char, do_tell_error, do_override;
	unsigned char do_verify, do_acknowledge;
	unsigned char do_blind_write;
	unsigned char mfc_id, dev_id;
	unsigned char boot_version, sec_bit;
	long flash_size, found;
	char *hexfile_name;
	long command, subcommand, beg_hi, end_lo;
	char hexstr[COUNT_HEXSTR_BUFF];
	ALineHEX_t *linehex;
}
ADeviceNXP_t;
/*----------------------------------------------------------------------------*/
int nxp_check_isp(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
int nxp_read_info(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
int nxp_check_data(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
int nxp_write_misc(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
int nxp_reset(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
int nxp_erase_block0(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
int nxp_blank_check(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
int nxp_device_info(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
int nxp_send_file(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
int nxp_verify_file(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
int nxp_send_blind(ASerialPort_t* aPort, ADeviceNXP_t* aDevice);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
