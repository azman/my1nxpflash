/*----------------------------------------------------------------------------*/
#include "my1nxp.h"
#include "my1cons.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
typedef int(*nxp_hexfunc_t)(ASerialPort_t*,ADeviceNXP_t*);
/*----------------------------------------------------------------------------*/
int nxp_check_isp(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	my1key_t key;
	char nxp_sync_char = aDevice ? aDevice->sync_char : NXP_SYNC_CHAR;
	int status = NXP_CHECK_START;
	flush_serial(aPort);
	purge_serial(aPort);
	while(status<NXP_CHECK_FOUND)
	{
		put_byte_serial(aPort,nxp_sync_char);
		wait_outgoing(aPort);
		if(check_incoming(aPort))
		{
			byte_t test = get_byte_serial(aPort);
			if(test==nxp_sync_char)
			{
				status = NXP_CHECK_FOUND;
				break;
			}
		}
		key = get_keyhit();
		if(key==KEY_ESCAPE)
			status = NXP_CHECK_ABORT;
		else if(key==KEY_BSPACE)
			status = NXP_CHECK_RESET;
	}
	flush_serial(aPort);
	purge_serial(aPort);
	return status;
}
/*----------------------------------------------------------------------------*/
char* nxp_create_hexstr(char* hexstr, int count, int addr, int type, int* data)
{
	int loop, checksum, addh, temp;
	count &= 0xFF;
	addh = (addr & 0xFF00)>>8;
	addr &= 0xFF;
	type &= 0xFF;
	checksum = count + addh + addr + type;
	sprintf(hexstr,":%02X%02X%02X%02X",count,addh,addr,type);
	for(loop=0;loop<count;loop++)
	{
		temp = data[loop] & 0xFF;
		checksum += temp;
		sprintf(hexstr,"%s%02X",hexstr,temp);
	}
	checksum = ~checksum + 1;
	sprintf(hexstr,"%s%02X",hexstr,(checksum&0xFF));
	return hexstr;
}
/*----------------------------------------------------------------------------*/
int nxp_send_hex(ASerialPort_t* aPort, const char* hexstr)
{
	byte_t test;
	int length = 0, error = 0, loop;
	while(hexstr[length]) length++;
#ifdef MY1DEBUG
	printf("Length=%d, HexStr='%s'\n",length,hexstr);
#endif
	purge_serial(aPort);
	for(loop=0;loop<length;loop++)
	{
		put_byte_serial(aPort, hexstr[loop]);
		while(!check_incoming(aPort));
		test = get_byte_serial(aPort);
		if(test!=hexstr[loop])
		{
#ifdef MY1DEBUG
			printf("Error: '%c' <=> '%c'\n",test,hexstr[loop]);
#endif
			error++;
		}
	}
	/* end with dos newline char pair */
	put_byte_serial(aPort, 0x0d); /* cr */
	put_byte_serial(aPort, 0x0a); /* lf */
	return error;
}
/*----------------------------------------------------------------------------*/
int nxp_get_wait(ASerialPort_t* aPort, char* aBuff, int aSize, int aWait)
{
	int count = 0;
	byte_t test;
	char *buff = malloc(aSize);
	while(!check_incoming(aPort));
	while(count<aWait||check_incoming(aPort))
	{
		while(!check_incoming(aPort));
		test = get_byte_serial(aPort);
#ifdef MY1DEBUG
		putchar(test);
#endif
		if(count<aSize-1)
			buff[count] = test;
		count++;
	}
	if(count<aSize) buff[count] = 0x0;
	else buff[aSize-1] = 0x0;
	sscanf(buff,"%s",aBuff);
#ifdef MY1DEBUG
	printf("BUFF:'%s', CHECK:%d/%d\n",aBuff,count,aWait);
#endif
	free(buff);
	return count < aSize ? 0 : count;
}
/*----------------------------------------------------------------------------*/
int nxp_read_info(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test = 0, data[READ_DATA_COUNT];
	if(aDevice->command!=NXP_ISP_READ)
		return NXP_ERROR_COMMAND;
	switch(aDevice->subcommand)
	{
		default:
		case READ_MFG_ID:
			data[0] = 0x00;
			data[1] = 0x00;
			break;
		case READ_DEV_ID:
			data[0] = 0x00;
			data[1] = 0x01;
			break;
		case READ_BOOT_VER:
			data[0] = 0x00;
			data[1] = 0x02;
			break;
		case READ_SEC_BIT:
			data[0] = 0x07;
			data[1] = 0x00;
			break;
	}
	nxp_create_hexstr(aDevice->hexstr,READ_DATA_COUNT,0,NXP_ISP_READ,data);
	if(aDevice->found) test = nxp_send_hex(aPort,aDevice->hexstr);
	return test;
}
/*----------------------------------------------------------------------------*/
int nxp_check_data(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test = 0, data[CHECK_DATA_COUNT];
	data[0] = (aDevice->beg_hi & 0xFF00)>>8;
	data[1] = aDevice->beg_hi & 0xFF;
	data[2] = (aDevice->end_lo & 0xFF00)>>8;
	data[3] = aDevice->end_lo & 0xFF;
	if(aDevice->command!=NXP_ISP_CHECK)
		return NXP_ERROR_COMMAND;
	switch(aDevice->subcommand)
	{
		default:
		case CHECK_BLANK:
			data[4] = 0x01;
			break;
		case CHECK_DATA:
			data[4] = 0x00;
			break;
	}
	nxp_create_hexstr(aDevice->hexstr,CHECK_DATA_COUNT,0,NXP_ISP_CHECK,data);
	if(aDevice->found) test = nxp_send_hex(aPort,aDevice->hexstr);
	return test;
}
/*----------------------------------------------------------------------------*/
int nxp_write_misc(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test = 0, data[WRITE_DATA_COUNT];
	if(aDevice->command!=NXP_ISP_WRITE)
		return NXP_ERROR_COMMAND;
	switch(aDevice->subcommand)
	{
		default:
		case WRITE_ERASE_BLOCK0:
			data[0] = 0x01;
			data[1] = 0x00;
			data[2] = 0x00;
			break;
		case WRITE_PROGRAM_SEC:
		case WRITE_PROGRAM_DCLK:
			data[0] = 0x05;
			data[1] = (aDevice->subcommand==WRITE_PROGRAM_SEC) ? 0x01 : 0x05;
			data[2] = 0x00;
			break;
		case WRITE_ERASE_SECTOR:
			data[0] = 0x08;
			data[1] = aDevice->beg_hi & 0xFF;
			data[2] = aDevice->end_lo & 0x80;
			break;
	}
	nxp_create_hexstr(aDevice->hexstr,WRITE_DATA_COUNT,0,NXP_ISP_WRITE,data);
	if(aDevice->found) test = nxp_send_hex(aPort,aDevice->hexstr);
	return test;
}
/*----------------------------------------------------------------------------*/
int nxp_reset(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test = 0;
	nxp_create_hexstr(aDevice->hexstr,0,0,NXP_ISP_RESET,0x0);
	if(aDevice->found) test = nxp_send_hex(aPort,aDevice->hexstr);
	return test;
}
/*----------------------------------------------------------------------------*/
int fget_line(char **pbuffer, int *pcount, FILE *pfile)
{
	int size;
	char *ptest;
	char readbuff[COUNT_HEXSTR_BUFF];
	/* read it! */
	ptest = fgets((char*)&readbuff, COUNT_HEXSTR_BUFF, pfile);
	if(!ptest) return -1; /* error... or, no more data? */
	/* no problem, got the string - count it! */
	size = strlen(readbuff);
	/* update buffer */
	if(size>*pcount)
	{
		ptest = realloc(*pbuffer, size);
		*pbuffer = ptest;
		*pcount = size;
	}
	/* copy in string */
	strcpy(*pbuffer, (char*)&readbuff);
	/* return size read */
	return size;
}
/*----------------------------------------------------------------------------*/
int get_hexbyte(const char* hexbyte)
{
	int value;
	char test[3] = { hexbyte[0], hexbyte[1], 0x0 };
	sscanf(test,"%02X",&value);
	return value&0xFF;
}
/*----------------------------------------------------------------------------*/
int get_hexword(const char* hexbyte)
{
	int value;
	char test[5] = { hexbyte[0], hexbyte[1], hexbyte[2], hexbyte[3], 0x0 };
	sscanf(test,"%04X",&value);
	return value&0xFFFF;
}
/*----------------------------------------------------------------------------*/
int nxp_check_hex(char* hexstr, ALineHEX_t* linehex)
{
	int length = 0, loop = 0, type;
	int count, address, checksum, test, temp;
	/* find length, filter newline! */
	while(hexstr[length])
	{
		linehex->hexstr[length] = hexstr[length];
		if(hexstr[length]=='\n'||hexstr[length]=='\r')
		{
			hexstr[length] = 0x0;
			linehex->hexstr[length] = 0x0;
			break;
		}
		length++;
		if(length>COUNT_HEXSTR_CHAR)
			return HEX_ERROR_LENGTH;
	}
	/* check valid length */
	if(length<9)
		return HEX_ERROR_LENGTH;
	/* check first char */
	if(hexstr[loop++]!=':')
		return HEX_ERROR_NOCOLON;
	/* get data count */
	count = get_hexbyte(&hexstr[loop]);
	checksum = count&0x0FF;
	loop += 2;
	/* get address - highbyte */
	test = get_hexbyte(&hexstr[loop]);
	checksum += test&0xFF;
	loop += 2;
	address = (test&0xFF)<<8;
	/* get address - lowbyte */
	test = get_hexbyte(&hexstr[loop]);
	checksum += test&0xFF;
	loop += 2;
	address |= (test&0xFF);
	/* get record type for checksum calc */
	type = get_hexbyte(&hexstr[loop]);
	checksum += type&0xFF;
	loop += 2;
	/* check EOF type? */
	if(type!=0x00&&type!=0x01)
		return HEX_ERROR_NOTDATA;
	/* save data if requested */
	if(linehex)
	{
		linehex->count = count;
		linehex->type = type;
		linehex->address = address;
		linehex->lastaddr = address + count - 1 + type; /* count=0 @ type=1! */
	}
	/* get data */
	for(temp=0;temp<count;temp++)
	{
		if(loop>=(length-2))
			return HEX_ERROR_LENGTH;
		test = get_hexbyte(&hexstr[loop]);
		if(linehex)
			linehex->data[temp] = test&0xFF;
		checksum += test&0xFF;
		loop += 2;
	}
	/* get checksum */
	if(loop!=(length-2))
		return HEX_ERROR_LENGTH;
	test = get_hexbyte(&hexstr[loop]);
	/* calculate and verify checksum */
	checksum = ~checksum + 1;
	if((test&0xFF)!=(checksum&0xFF))
		return HEX_ERROR_CHECKSUM;
	/* returns record type */
	return type;
}
/*----------------------------------------------------------------------------*/
int nxp_check_file(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int line = 0, error = 0;
	int test, count, size = COUNT_HEXSTR_BUFF*sizeof(char);
	char *buff = malloc(size);
	FILE *pfile_hex = fopen(aDevice->hexfile_name,"rt");
	if(!pfile_hex)
	{
		error = HEX_ERROR_FILE;
	}
	else
	{
		/* check line by line! */
		while((count=fget_line(&buff,(int*)&size,pfile_hex))!=-1)
		{
			if(size<count)
				size = count;
			line++;
			test = nxp_check_hex(buff,aDevice->linehex);
			if(test<0)
			{
				error = test;
				break;
			}
			else if(aDevice->linehex->lastaddr>=aDevice->flash_size)
			{
				error = HEX_ERROR_OVERFLOW;
				break;
			}
		}
		fclose(pfile_hex);
	}
	free(buff);
	if(error&&aDevice->do_tell_error)
	{
		switch(error)
		{
			case HEX_ERROR_FILE:
				printf("Cannot open HEX file '%s'!",aDevice->hexfile_name);
				break;
			case HEX_ERROR_LENGTH:
				printf("HEX string length error @line %d!\n",line);
				break;
			case HEX_ERROR_NOCOLON:
				printf("HEX string no ':' @line %d!\n",line);
				break;
			case HEX_ERROR_NOTDATA:
				printf("HEX record type NOT supported @line %d!\n",line);
				break;
			case HEX_ERROR_CHECKSUM:
				printf("HEX record checksum error @line %d!\n",line);
				break;
			case HEX_ERROR_OVERFLOW:
				printf("HEX record address device overflow @line %d!\n",line);
				break;
			default:
				printf("Failed HEX check @line %d!\n",line);
		}
	}
	return error ? error : line;
}
/*----------------------------------------------------------------------------*/
int nxp_device_hex(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	/* wrapper to enable function pointer */
	return nxp_send_hex(aPort,aDevice->linehex->hexstr);
}
/*----------------------------------------------------------------------------*/
int nxp_verify_hex(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test, loop, wait, count, index = 0, length = 0;
	char hexchk[COUNT_HEXSTR_BUFF];
	/* no need to check end-of-record line! */
	if(aDevice->linehex->type==0x01)
		return 0;
	aDevice->command = NXP_ISP_CHECK;
	aDevice->subcommand = CHECK_DATA;
	aDevice->beg_hi = aDevice->linehex->address;
	aDevice->end_lo = aDevice->linehex->address+aDevice->linehex->count-1;
	count = aDevice->linehex->count*2;
	wait = NXP_CHECK_EQ_INDEX+1+NXP_CHECK_BUFF_EXT+count;
	/* send data read command */
	test = nxp_check_data(aPort, aDevice);
	if(test) return HEX_ERROR_GENERAL;
	/* wait for data */
	test = nxp_get_wait(aPort,hexchk,COUNT_HEXSTR_BUFF,wait);
	if(test) return HEX_ERROR_ACK;
	/* get length */
	while(hexchk[length]) length++;
	if(length<NXP_CHECK_EQ_INDEX+1) return HEX_ERROR_LENGTH;
#ifdef MY1DEBUG
	printf("[DEBUG0] {%s},{%s}\n",hexchk,aDevice->linehex->hexstr);
#endif
	/* check address */
	test = get_hexword(&hexchk[index]);
	if(test!=aDevice->linehex->address) return HEX_ERROR_INVALID;
	/* check '=' sign */
	if(hexchk[NXP_CHECK_EQ_INDEX]!='=') return HEX_ERROR_INVALID;
	/* check data */
	index=NXP_CHECK_EQ_INDEX+1;
	if((length-index)<count) return HEX_ERROR_LENGTH;
#ifdef MY1DEBUG
	printf("[DEBUGX] ");
#endif
	count = 0; /* now, count error */
	for(loop=0;loop<aDevice->linehex->count;loop++)
	{
		test = get_hexbyte(&hexchk[index]);
		if(test!=aDevice->linehex->data[loop])
			count++;
		index += 2;
#ifdef MY1DEBUG
		printf("([%02X|%02X]{%d})",test,aDevice->linehex->data[loop],count);
#endif
	}
#ifdef MY1DEBUG
	printf("\n");
#endif
	return count;
}
/*----------------------------------------------------------------------------*/
int nxp_wait_byte_ack(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int error, value = -1, length = 0;
	aDevice->do_acknowledge = 0;
	error = nxp_get_wait(aPort,aDevice->hexstr,COUNT_HEXSTR_BUFF,3);
	if(error&&aDevice->do_tell_error)
	{
		printf("[ACK ERROR] Surplus chars (%d)\n", error);
		return -error;
	}
	while(aDevice->hexstr[length]) length++;
	if(aDevice->hexstr[length-1]=='.')
	{
		aDevice->hexstr[length-1] = 0x0;
		sscanf(aDevice->hexstr,"%x",&value);
		aDevice->do_acknowledge = 1;
	}
	return value;
}
/*----------------------------------------------------------------------------*/
int nxp_wait_acknowledge(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int error;
	aDevice->do_acknowledge = 0;
	error = nxp_get_wait(aPort,aDevice->hexstr,COUNT_HEXSTR_BUFF,0);
	if(error&&aDevice->do_tell_error)
	{
		printf("[ACK ERROR] Surplus chars (%d)\n", error);
		return error;
	}
	if(!strcmp(aDevice->hexstr,"."))
		aDevice->do_acknowledge = 1;
	return 0;
}
/*----------------------------------------------------------------------------*/
int nxp_erase_block0(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int error = 0;
	aDevice->command = NXP_ISP_WRITE;
	aDevice->subcommand = WRITE_ERASE_BLOCK0;
	error = nxp_write_misc(aPort, aDevice);
	if(error&&aDevice->do_tell_error)
	{
		printf("[CMD ERROR] Failed sending command (%d)\n", error);
		return error;
	}
	return nxp_wait_acknowledge(aPort,aDevice);
}
/*----------------------------------------------------------------------------*/
int nxp_blank_check(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int error = 0;
	aDevice->command = NXP_ISP_CHECK;
	aDevice->subcommand = CHECK_BLANK;
	error = nxp_check_data(aPort, aDevice);
	if(error&&aDevice->do_tell_error)
	{
		printf("[CMD ERROR] Failed sending command (%d)\n", error);
		return error;
	}
	return nxp_wait_acknowledge(aPort,aDevice);
}
/*----------------------------------------------------------------------------*/
int nxp_device_info(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int test, value;
	/* get manufacturing id */
	aDevice->command = NXP_ISP_READ;
	aDevice->subcommand = READ_MFG_ID;
	test = nxp_read_info(aPort,aDevice);
	if(test) return test;
	value = nxp_wait_byte_ack(aPort,aDevice);
	if(value<0) return -value;
	aDevice->mfc_id = value&0xFF;
	/* get device id */
	aDevice->subcommand = READ_DEV_ID;
	test = nxp_read_info(aPort,aDevice);
	if(test) return test;
	value = nxp_wait_byte_ack(aPort,aDevice);
	if(value<0) return -value;
	aDevice->dev_id = value&0xFF;
	/* get boot version */
	aDevice->subcommand = READ_BOOT_VER;
	test = nxp_read_info(aPort,aDevice);
	if(test) return test;
	value = nxp_wait_byte_ack(aPort,aDevice);
	if(value<0) return -value;
	aDevice->boot_version = value&0xFF;
	/* get security bit */
	aDevice->subcommand = READ_SEC_BIT;
	test = nxp_read_info(aPort,aDevice);
	if(test) return test;
	value = nxp_wait_byte_ack(aPort,aDevice);
	if(value<0) return -value;
	aDevice->sec_bit = value&0xFF;
	/* need to get extra ack? */
	nxp_wait_acknowledge(aPort,aDevice);
	return 0;
}
/*----------------------------------------------------------------------------*/
int nxp_send_file(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	ALineHEX_t linehex;
	char hexchk[COUNT_HEXSTR_BUFF];
	int linecount, line = 0, error = 0;
	int count, size = COUNT_HEXSTR_CHAR*sizeof(char);
	char *buff = malloc(size);
	FILE *pfile_hex;
	nxp_hexfunc_t pfunc;
	/* prepare links! */
	linehex.hexstr = hexchk;
	aDevice->linehex = &linehex;
	/* get line count and check for error! */
	linecount = nxp_check_file(aPort,aDevice);
	if(linecount<0) return linecount;
	/* check for verification request */
	if(aDevice->do_verify) pfunc = nxp_verify_hex;
	else pfunc = nxp_device_hex;
	aDevice->do_verify = 0;
	/* do the thing! */
	pfile_hex = fopen(aDevice->hexfile_name,"rt");
	if(!pfile_hex) return HEX_ERROR_FILE;
	printf("000/%03u\b\b\b\b",linecount);
	while((count=fget_line(&buff,(int*)&size,pfile_hex))!=-1)
	{
		if(size<count)
			size = count;
		line++;
		/* check valid hex */
		nxp_check_hex(buff,&linehex);
		/* do the requested process */
		if((error=pfunc(aPort,aDevice))!=0)
			break;
		printf("\b\b\b%03u",line); fflush(stdout);
	}
	fclose(pfile_hex);
	free(buff);
	printf("\b\b\b       \b\b\b\b\b\b\b");
	if(error)
	{
		printf("Failed @line %d (%d)!\n",line,error);
		return error;
	}
	if(pfunc==nxp_verify_hex)
		return 0;
	error = nxp_wait_acknowledge(aPort,aDevice);
	if(!aDevice->do_acknowledge)
	{
		printf("[ACK ERROR] NOT FOUND!\n");
		error++;
	}
	/* need to get extra ack? */
	nxp_wait_acknowledge(aPort,aDevice);
	return error;
}
/*----------------------------------------------------------------------------*/
int nxp_verify_file(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	aDevice->do_verify = 0x1;
	return nxp_send_file(aPort,aDevice);
}
/*----------------------------------------------------------------------------*/
int nxp_send_blind(ASerialPort_t* aPort, ADeviceNXP_t* aDevice)
{
	int linecount, line = 0;
	int loop, count, size = COUNT_HEXSTR_CHAR*sizeof(char);
	char *buff = malloc(size);
	FILE *pfile_hex;
	printf("... ");
	fflush(stdout);
	/* do the thing! */
	pfile_hex = fopen(aDevice->hexfile_name,"rt");
	if(!pfile_hex) return HEX_ERROR_FILE;
	/* get line count */
	linecount = 0;
	while((count=fget_line(&buff,(int*)&size,pfile_hex))!=-1)
		linecount++;
	/* reset */
	fseek(pfile_hex,0,SEEK_SET);
	printf("000/%03u\b\b\b\b",linecount);
	fflush(stdout);
	while((count=fget_line(&buff,(int*)&size,pfile_hex))!=-1)
	{
		line++;
		/* start sending */
		purge_serial(aPort);
		for(loop=0;loop<count;loop++)
			put_byte_serial(aPort, buff[loop]);
		/* end with dos newline char pair */
		put_byte_serial(aPort, 0x0d); /* cr */
		put_byte_serial(aPort, 0x0a); /* lf */
		/* update line info */
		printf("\b\b\b%03u",line);
		fflush(stdout);
	}
	fclose(pfile_hex);
	free(buff);
	printf("\b\b\b       \b\b\b\b\b\b\b");
	/* purge input */
	purge_serial(aPort);
	return 0;
}
/*----------------------------------------------------------------------------*/
